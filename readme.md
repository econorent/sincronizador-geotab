# Actualizador base de datos

La función de este servicio, es mantener actualizada la información en la base de datos OPERACIONES, realizando el calculo de empleados que incumplen la norma de exceso de velocidad en un contrato dado.

  Este servicio depende de los siguientes elementos:

- **Geotab**
- **ODBC** que permita la conexión a la base de datos 4D
- **Nodejs**

### Lógica del servicio.

  La primera tarea que ejecuta el servicio es conectarse a la base de datos 4D, para extraer los contratos existentes en la tabla "EntSal". Se obtienen tanto contratos de salida como los de entrada, luego se asocian por medio del campo "NoRental" y el calculo de las fechas de entrada y salida.

  Una vez obtenidos los datos de los contratos, se procede a conectar con "Geotab" para obtener los registros de los vehículos.

  Primero obtenemos los dispositivos (o vehículos), y luego obtenemos todas las excepciones de un día específico referentes a exceso de velocidad.

  Procedemos a asociar las excepciones a los contratos que correspondan, y descartamos las que no pertenezcan a algún contrato. Luego de tener las excepciones asociadas se solicitan los logs de cada excepcion para obtener la velocidad y posición geográfica de la excepción. Se procede a invocar la función "GetAddresses" de Geotab para tomar los datos de dicha ubicación.

  Una vez obtenida esta información se asocian los datos restantes, y se procede a eliminar todo registro en la tabla "GeotabExceptions" para registrar los valores recien calculados.

### Instalación

El primer paso es configurar el ODBC para conectar a la base de datos 4D.

- Ejecutamos el administrador de origines de datos ODBC 64-bit
- En la pestaña DNS del sistema agregamos un nuevo origen de datos.
- Seleccionar la opción 4D v17 Rx ODBC Driver 64-bit, y presionar finalizar.
- Se muestra una nueva ventana de configuración donde se colocaran los siguientes datos:
    * Data Source Name: ALQUILER4D
    * Description: 4D 64 bits ODBC Datasource
    * Server address: <IP de servidor 4D>
    * User: <Usuario de conexión 4D>
    * Password: <Contraseña de acceso 4D>
    * Marcar el boton "Advanced <<"
    * Seleccionar "compatibility" la casilla "Open Query".
    * Comprobar que la conexión funciona correctamente con el boton "Test..."
    * "Ok" para guardar y finalizar.

El servicio requiere la instalación [Node.js](https://nodejs.org/) V8 o superior para su ejecución.

accedemos a la carpeta raíz del servicio por medio de la consola de comandos.

```sh
$ cd GeotabDatabaseUpdate
```

crear un nuevo archivo de variables de entorno

```sh
$ copy .env.example .env
```

Configurar los valores

- APP_ENV: indica el entorno   
    
    * DEVELOPMENT: entorno de desarrollo.
    * TEST: entorno de pruebas.
    * PRODUCTION: entorno de producción.

- ODBC_CONNECTION_STRING: indica la configuracion de ODBC que utilizara el servicio para conectar a la base de datos 4D

- Para configurar los valores de conexión de geotab:
    
    * GEOTAB_USER: usuario.
    * GEOTAB_PASSWORD: contraseña.
    * GEOTAB_SESSION_ID: valor nulo por defecto.
    * GEOTAB_DATABASE: nombre de base de datos.
    * GEOTAB_SERVER: servidor.

- el archivo permite configurar las conexiones a distintos servidores de base de datos, ya sea para desarrollo, pruebas o producción. Sin embargo el valor de APP_ENV es quien determina que configuración se utilizara:

    * {enviroment}_DB_HOST: nombre del servidor.
    * {enviroment}_DB_INSTANCE: instancia de base de datos.
    * {enviroment}_DB_NAME: nombre de base de datos.
    * {enviroment}_DB_PASSWORD: contraseña de usuario.
    * {enviroment}_DB_PORT: puerto de conexión.
    * {enviroment}_DB_USER: usuario.
    * {enviroment}_DB_DIALECT: dialecto de base de datos.
   
Se procede a instalar las dependencias del servicio.

```sh
$ npm run install 
``` 

Ejecutamos la migración de datos para crear las tablas necesarias en DB.

```sh
$ npm run migrate 
```

Se debe crear una tarea de windows para que el servicio se ejecute siempre, e inicie junto con el arranque del sistema. Hay que asegurarse de cambiar la ruta de ubicacion en el siguiente script

```sh
$ schtasks /create /SC ONSTART /TN sincronizador-excepciones /TR "<RUTA DE APLICATIVO>\app.vbs" /RL HIGHEST 
```

Podemos iniciar el servicio manualmente desde el administrador de tareas de windows, buscando por el nombre "sincrionizador" o con el siguiente comando:

```shell script
$ schtasks /run /tn sincronizador
```

Una vez realizada la configuración el servicio se encargará de actualizar constantemente la base de datos con los registros de los casos de exceso de velocidad causados por empleados de ECONORENT.

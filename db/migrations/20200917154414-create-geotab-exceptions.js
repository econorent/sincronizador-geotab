
const DB = require('../index');
const db = new DB();

async function initModel() {
  await db.init();
  return db.db.GeotabExceptions;
}

module.exports = {
  up: async (queryInterface) => {
    const model = await initModel();

    await queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
      uniqueKeys: model.uniqueKeys,
    });
  },

  down: async (queryInterface) => {
    const model = await initModel();

    await queryInterface.dropTable(model.name);
  },
};

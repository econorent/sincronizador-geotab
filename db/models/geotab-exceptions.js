
module.exports = (sequelize, DataTypes) => {
  const structure = {
    NoRental: {
      allowNull: false,
      comment: 'Codigo de contrato norental.',
      type: 'varchar(50)',
    },
    Auto: {
      allowNull: false,
      comment: 'Identificador del vehículo.',
      type: 'varchar(20)',
    },
    FechaOut: {
      allowNull: false,
      comment: 'Fecha de salida del vehículo.',
      type: 'datetime',
    },
    FechaIn: {
      allowNull: false,
      comment: 'Fecha de entrada del vehículo.',
      type: 'datetime',
    },
    EmpleId: {
      allowNull: false,
      comment: 'Codigo del empleado.',
      type: DataTypes.INTEGER,
    },
    Nombre: {
      allowNull: false,
      comment: 'Nombre del chofer.',
      type: 'varchar(50)',
    },
    Apellido: {
      allowNull: false,
      comment: 'Apellido del chofer.',
      type: 'varchar(50)',
    },
    AgenciaOrigen: {
      allowNull: true,
      comment: 'Agencia origen del viaje.',
      type: 'varchar(50)',
    },
    AgenciaDestino: {
      allowNull: true,
      comment: 'Agencia destino del viaje.',
      type: 'varchar(50)',
    },
    FechaExcepcion: {
      allowNull: false,
      comment: 'Fecha de la excepción.',
      type: 'datetime',
    },
    DistanciaExcepcion: {
      allowNull: false,
      comment: 'Distancia recorrida durante la excepción.',
      type: DataTypes.FLOAT,
    },
    DuracionExcepcion: {
      allowNull: false,
      comment: 'Tiempo que duro la excepción.',
      type: 'varchar(50)',
    },
    VelocidadMaximaViaje: {
      allowNull: true,
      comment: 'Velocidad maxima alcanzada durante el viaje.',
      type: DataTypes.INTEGER,
    },
    ValocidadMaximaRuta: {
      allowNull: false,
      comment: 'Limite de velocidad de la ruta.',
      type: DataTypes.INTEGER,
    },
    VelocidadMaximaExcepcion: {
      allowNull: false,
      comment: 'Velocidad maxima alcanzada durante la excepción.',
      type: DataTypes.INTEGER,
    },
    TramoExcepcion: {
      allowNull: false,
      comment: 'Coordenadas de la excepción.',
      type: 'varchar(150)',
    },
  };

  const model = sequelize.define('GeotabExceptions', structure, {
    comment: 'Excepciones de los dispositivos registrados en geotab.',
  });

  model.structure = structure;

  model.removeAttribute('id');

  model.associate = () => {};

  return model;
};

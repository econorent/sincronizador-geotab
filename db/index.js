const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const basename = path.basename(__filename);
const config = require('./config')[process.env.APP_ENV.toLowerCase()];

class DB {
  db = {
    Op: Sequelize.Op,
    Sequelize,
    sequelize: null,
  };

  constructor() {}

  static test = null;

  async init() {
    this.sequelize = new Sequelize(config);

    await this.sequelize.authenticate();
    console.info('La conexión se ha establecido correctamente.');

    this.db.sequelize = this.sequelize;

    fs
      .readdirSync(path.join(__dirname, 'models'))
      .forEach((file) => {
        if (file.indexOf('.') !== 0 && file !== basename && path.extname(file) === '.js') {
          const model = require(path.join(__dirname, 'models', file))(this.sequelize, Sequelize, fs, path, moment);

          this.db[model.name] = model;
        }
      });
  }
}

module.exports = DB;

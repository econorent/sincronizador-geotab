
const path = require('path');

require('dotenv').config({
  path: path.join(__dirname, '..', '.env'),
});

module.exports = {
  development: {
    database: process.env.DEVELOPMENT_DB_NAME,
    dialect: process.env.DEVELOPMENT_DB_DIALECT,
    dialectOptions: {
      options: {
        instanceName: process.env.DEVELOPMENT_DB_INSTANCE,
        validateBulkLoadParameters: true,
      },
      requestTimeout: 30000,
    },
    host: process.env.DEVELOPMENT_DB_HOST,
    instance: process.env.DEVELOPMENT_DB_INSTANCE,
    migrationStorage: process.env.DB_MIGRATION_STORAGE,
    migrationStoragePath: process.env.DB_MIGRATION_STORAGE_PATH,
    migrationStorageTableName: process.env.DB_MIGRATION_STORAGE_TABLE,
    password: process.env.DEVELOPMENT_DB_PASSWORD,
    port: process.env.DEVELOPMENT_DB_PORT,
    seederStorage: process.env.DB_SEEDER_STORAGE,
    seederStoragePath: process.env.DB_SEEDER_STORAGE_PATH,
    seederStorageTableName: process.env.DB_SEEDER_STORAGE_TABLE,
    username: process.env.DEVELOPMENT_DB_USER,
    define: {
      freezeTableName: true,
      timestamps: false,
    },
    pool: {
      max: 50,
      min: 0,
      idle: 10000,
    },
    validateBulkLoadParameters: true,
    logging: false,
  },
  test: {
    database: process.env.TEST_DB_NAME,
    dialect: process.env.TEST_DB_DIALECT,
    dialectOptions: {
      options: {
        instanceName: process.env.TEST_DB_INSTANCE,
        validateBulkLoadParameters: true,
      },
      requestTimeout: 30000,
    },
    host: process.env.TEST_DB_HOST,
    instance: process.env.TEST_DB_INSTANCE,
    migrationStorage: process.env.DB_MIGRATION_STORAGE,
    migrationStoragePath: process.env.DB_MIGRATION_STORAGE_PATH,
    migrationStorageTableName: process.env.DB_MIGRATION_STORAGE_TABLE,
    password: process.env.TEST_DB_PASSWORD,
    port: process.env.TEST_DB_PORT,
    seederStorage: process.env.DB_SEEDER_STORAGE,
    seederStoragePath: process.env.DB_SEEDER_STORAGE_PATH,
    seederStorageTableName: process.env.DB_SEEDER_STORAGE_TABLE,
    username: process.env.TEST_DB_USER,
    define: {
      freezeTableName: true,
      timestamps: false,
    },
    pool: {
      max: 50,
      min: 0,
      idle: 10000,
    },
    validateBulkLoadParameters: true,
    logging: false,
  },
  production: {
    database: process.env.PRODUCTION_DB_NAME,
    dialect: process.env.PRODUCTION_DB_DIALECT,
    dialectOptions: {
      options: {
        instanceName: process.env.PRODUCTION_DB_INSTANCE,
        validateBulkLoadParameters: true,
      },
      requestTimeout: 30000,
    },
    host: process.env.PRODUCTION_DB_HOST,
    instance: process.env.PRODUCTION_DB_INSTANCE,
    migrationStorage: process.env.DB_MIGRATION_STORAGE,
    migrationStoragePath: process.env.DB_MIGRATION_STORAGE_PATH,
    migrationStorageTableName: process.env.DB_MIGRATION_STORAGE_TABLE,
    password: process.env.PRODUCTION_DB_PASSWORD,
    port: process.env.PRODUCTION_DB_PORT,
    seederStorage: process.env.DB_SEEDER_STORAGE,
    seederStoragePath: process.env.DB_SEEDER_STORAGE_PATH,
    seederStorageTableName: process.env.DB_SEEDER_STORAGE_TABLE,
    username: process.env.PRODUCTION_DB_USER,
    define: {
      freezeTableName: true,
      timestamps: false,
    },
    pool: {
      max: 50,
      min: 0,
      idle: 10000,
    },
    validateBulkLoadParameters: true,
    logging: false,
  },
};

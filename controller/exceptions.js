
const Main = require('../bin/main');

/** Obtiene las excepciones de los empleados. */
class DriverExceptions extends Main {
  /**
   * Instancia la clase DriverExceptions e inicializa .
   * @param {object} geotab - instancia de servicio de geotab.
   */
  constructor(geoTab) {
    super();
    this.conn = null;
    this.contracts = [];
    this.contractsResult = null;
    this.contractsWithExceptions = [];
    this.dateEnd = null;
    this.dateStart = null;
    this.devices = null;
    this.devicesException = null;
    this.devicesExceptionSearchData = [];
    this.ruleData = null;
    this.geoTab = new geoTab();
  }

  /**
   * Determina si el proceso se ejecutara en un rango de fechas o se ejecutara siempre en la fecha actual.
   */
  async init() {
    if (
      process.argv[2]
      && this.moment(process.argv[2], process.env.DATE_FORMAT_SHORT, true).isValid()
      && process.argv[3]
      && this.moment(process.argv[3], process.env.DATE_FORMAT_SHORT, true).isValid()
    ) {
      await this.start();
    } else {
      await this.startAsync();
    }
  }

  /**
   * si el proceso inicia con un rango de fecha, se ejecutara el procedimiento una vez en cada una de las fechas
   * calculadas en dicho rango.
   */
  async start() {
    const dates = this.getDates(
      this.moment(process.argv[2], process.env.DATE_FORMAT_SHORT),
      this.moment(process.argv[3], process.env.DATE_FORMAT_SHORT)
    );
    try {
      await this.geoTab.init();

      for (const date of dates) {
        this.dateStart = date.clone().startOf('date');
        this.dateEnd = date.clone().endOf('date');

        this.logger.info('Consulta de registros: %s', this.dateStart.format(process.env.DATE_FORMAT_SHORT));

        await this.findExceptions();

        if (!this.contractsWithExceptions.length) {
          this.logger.info('Nada que guardar.\n\n');

          await this.timeout();
          continue;
        }

        await this.saveExceptions();

        this.logger.info('Guardadas %s excepciones.\n\n', this.contractsWithExceptions.length);

        await this.timeout();
      }

      this.logger.info('Finalizo guardado masivo.\n\n');
    } catch (error) {
      this.logger.error({
        status: error.status,
        message: error.message,
      });
    }
  }

  /**
   * Si no se indica el rango de fecha, el proceso se ejecutara continuamente en un ciclo infinito.
   */
  async startAsync() {
    // eslint-disable-next-line no-constant-condition
    while (true) {
      try {
        await this.geoTab.init();

        const dates = this.getDates(this.moment(), this.moment());
        this.dateStart = dates[0].clone().startOf('date');
        this.dateEnd = dates[0].clone().endOf('date');

        this.logger.info('Consulta de registros');

        await this.findExceptions();

        if (!this.contractsWithExceptions.length) {
          this.logger.info('Nada que guardar.\n\n');
          continue;
        }

        await this.saveExceptions();

        this.logger.info('Registros guardados el.\n\n');
      } catch (error) {
        this.logger.error(error.message);
      } finally {
        await this.timeout();
      }
    }
  }

  /**
   * calcula las excepciones de los empleados segun su contrato de salida y entrada.
   * @returns {Promise<void>}
   */
  async findExceptions() {
    try {
      [
        this.ruleData,
        this.devices,
        this.contractsResult,
      ] = await Promise.all([
        this.geoTab.GetGeoTabRule('Exceso de velocidad'),
        this.geoTab.GetGeoTabDevices(),
        this.getContracts(),
      ]);

      await this.combineContracts(this.contractsResult);

      this.devicesException = await this.geoTab.GetMultiCall(this.devicesExceptionSearchData);

      await this.filterExceptions();

      if (!this.contractsWithExceptions.length) {
        return;
      }

      this.logger.info('Obteniendo log respectivo a cada excepción.');

      await this.async.mapLimit(this.contractsWithExceptions, 10, async (exception) => {
        const findLogs = await this.geoTab.GetLogRecord(
          exception.FechaExcepcion,
          exception.FechaFinExcepcion,
          exception.device
        );
        const max = this._.maxBy(findLogs, 'speed');
        const address = await this.geoTab.GetAddresses(max.latitude, max.longitude);
        const roadMaxSpeed = await this.geoTab.GetRoadMaxSpeeds(
          exception.device,
          max.dateTime,
          max.dateTime
        );

        for (const i in roadMaxSpeed) {
          if (roadMaxSpeed[i].v <= 0 || roadMaxSpeed[i].v >= max.speed) {
            delete roadMaxSpeed[i];
            continue;
          }

          if (roadMaxSpeed[i].k < max.dateTime) {
            roadMaxSpeed[i].diff = this.moment(max.dateTime).diff(roadMaxSpeed[i].k, 'seconds');
          } else if (roadMaxSpeed[i].k > max.dateTime) {
            roadMaxSpeed[i].diff = this.moment(roadMaxSpeed[i].k).diff(max.dateTime, 'seconds');
          }
        }

        exception.ValocidadMaximaRuta = (this._.minBy(roadMaxSpeed, 'diff')).v;
        exception.VelocidadMaximaExcepcion = max.speed;
        exception.FechaExcepcion = this.moment(exception.FechaExcepcion)
          .local()
          .format(process.env.DATE_FORMAT_LONG);
        exception.FechaFinExcepcion = this.moment(exception.FechaFinExcepcion)
          .local()
          .format(process.env.DATE_FORMAT_LONG);
        exception.TramoExcepcion = address[0].formattedAddress;

        delete exception.device;
      });

      this.logger.info('Logs asociados.');
    } catch (error) {
      throw new this.ErrorEnhanced(500, error);
    }
  }

  /**
   *
   * Obtiene todos los contratos de salida y entrada de 4D que pertenezcan a empleados de ECONORENT
   * @returns {Promise<{in: null, out: null}>}
   */
  async getContracts() {
    const dateIn = this.dateStart.format('YYYY-MM-DD');
    const dateOut = this.dateEnd.format('YYYY-MM-DD');
    const contractsResponse = {
      in: null,
      out: null,
    };

    this.logger.info('Obteniendo contratos.');

    this.conn = await this.odbc.connect(process.env.ODBC_CONNECTION_STRING);

    for (const el of [
      { contract: 'out', columName: 'horaout', inout: false, },
      { contract: 'in', columName: 'horain', inout: true, },
    ]) {
      const queryStringArray = [
        'SELECT',
        'EntSal.norental AS norental,',
        'emple.unikey AS empleid,',
        'EntSal.auto AS auto,',
        'EntSal.inout AS inout,',
        'EntSal.fecha AS fecha,',
        `EntSal.hora AS ${ el.columName },`,
        'emple.nombre AS nombre,',
        'emple.apellido AS apellido,',
        'agen.nombre AS agencia',
        'FROM EntSal',
        'JOIN emple ON entsal.Emple = emple.unikey',
        'JOIN agen ON entsal.agencia = agen.unikey',
        'WHERE EntSal.contrato = 0',
        'AND EntSal.GDesp = 0',
        'AND  entsal.norental <> 0',
        'AND emple.unikey <> 0',
        `AND inout = ${ el.inout }`,
        `AND EntSal.fecha between '${ dateIn }' AND '${ dateOut }'`,
        'ORDER BY EntSal.hora DESC',
      ];

      const result = await this.conn.query(queryStringArray.join(' '));
      contractsResponse[el.contract] = result;
    }

    await this.conn.close();

    this.logger.info('Obtenidos %s contratos de salida.', contractsResponse.out.length);
    this.logger.info('Obtenidos %s contratos de entrada.', contractsResponse.in.length);

    return contractsResponse;
  }

  /**
   * Asocia los contratos por medio de la salida y entrada, de esta manera filtramos los mismos.
   * @param {object} contractsData - contratos de entrada y salida obtenidos
   * @returns {Promise<void>}
   */
  async combineContracts(contractsData) {
    this.devicesExceptionSearchData = [];
    this.contracts = [];

    for (const contractOut of contractsData.out) {
      const device = await this._.find(this.devices, (device) => {
        return device.name === contractOut.auto;
      });

      if (!isNaN(contractOut.horaout)) {
        contractOut.horaout = this.moment.utc(new Date(parseInt(contractOut.horaout, 10)));
        contractOut.horaout
          = contractOut.horaout.hours() + ':' + contractOut.horaout.minutes() + ':' + contractOut.horaout.seconds();
      }

      const contractIn = await this._.find(contractsData.in, (contractIn) => {
        if (!isNaN(contractIn.horain)) {
          contractIn.horain = this.moment.utc(new Date(parseInt(contractIn.horain, 10)));
          contractIn.horain
            = contractIn.horain.hours() + ':' + contractIn.horain.minutes() + ':' + contractIn.horain.seconds();
        }

        return contractIn.norental === contractOut.norental
          && contractOut.auto === contractIn.auto
          && contractOut.empleid === contractIn.empleid
          && this.moment(contractOut.horaout, 'hh:mm:ss')
            .isBefore(this.moment(contractIn.horain, 'hh:mm:ss'), 'minute');
      });

      if (device && contractIn) {
        const FechaOut = this.moment(contractOut.fecha.split(' ')[0] + contractOut.horaout, 'DD/MM/YYYY HH:mm:ss')
          .format(process.env.DATE_FORMAT_LONG);
        const FechaIn = this.moment(contractIn.fecha.split(' ')[0] + contractIn.horain, 'DD/MM/YYYY HH:mm:ss')
          .format(process.env.DATE_FORMAT_LONG);

        this.devicesExceptionSearchData.push([
          'Get',
          {
            typeName: 'ExceptionEvent',
            search: {
              devicesSearch: { id: device.id, },
              ruleSearch: { id: this.ruleData.id, },
              fromDate: FechaOut,
              toDate: FechaIn,
            },
          },
        ]);

        this.contracts.push({
          device,
          NoRental: contractOut.norental,
          EmpleId: contractOut.empleid,
          Auto: contractOut.auto,
          FechaOut,
          FechaIn,
          Nombre: contractOut.nombre,
          Apellido: contractOut.apellido,
          AgenciaOrigen: contractOut.agencia,
          AgenciaDestino: contractIn.agencia,
          FechaExcepcion: null,
          DistanciaExcepcion: null,
          DuracionExcepcion: null,
          VelocidadMaximaViaje: null,
          VelocidadMaximaExcepcion: null,
          TramoExcepcion: null,
        });
      }
    }

    this.logger.info('Total de contratos: %s.', this.contracts.length);
  }

  /**
   * Filtrar y asociar excepciones a su respectivo contrato.
   * @returns {Promise<void>}
   */
  async filterExceptions() {
    this.contractsWithExceptions = [];

    for (const contract of this.contracts) {
      const exception = await this._.find(this.devicesException, (e) => {
        return contract.device.id === e.device.id
          && this.moment(e.activeFrom, process.env.DATE_FORMAT_LONG).local()
            .isBetween(contract.FechaOut, contract.FechaIn);
      });

      if (exception) {
        contract.FechaExcepcion = exception.activeFrom;
        contract.FechaFinExcepcion = exception.activeTo;
        contract.DistanciaExcepcion = exception.distance;
        contract.DuracionExcepcion = exception.duration;
        contract.VelocidadMaximaViaje = null;
        contract.VelocidadMaximaExcepcion = null;
        contract.TramoExcepcion = null;

        this.contractsWithExceptions.push(contract);
      }
    }

    this.logger.info('Excepciones asociadas a su contrato.');
  }

  async saveExceptions() {
    const transaction = await this.db.sequelize.transaction();

    try {
      await this.db.GeotabExceptions.destroy({
        where: {
          FechaOut: {
            [this.db.Op.between]: [
              this.dateStart.format(process.env.DATE_FORMAT_LONG),
              this.dateEnd.format(process.env.DATE_FORMAT_LONG),
            ],
          },
        },
        transaction,
      });

      await this.db.GeotabExceptions
        .bulkCreate(this.contractsWithExceptions, {
          transaction,
        });

      await transaction.commit();

      this.logger.info('Registros guardados con éxito.');
    } catch (error) {
      await transaction.rollback();

      throw new this.ErrorEnhanced(500, error);
    }
  }

  /**
   * Calcular el rango de fechas para trabajar.
   * @param {object(moment)} dateStart - fecha de inicio.
   * @param {object(moment)} dateEnd - fecha de termino.
   * @returns {array}
   */
  getDates(dateStart, dateEnd) {
    const dates = [];

    if (!dateStart.isSameOrBefore(dateEnd)) {
      throw new this.ErrorEnhanced(500, 'Error en las fechas indicadas.');
    }

    this._.times(dateEnd.diff(dateStart, 'days') + 1).forEach((item) => {
      dates.push(dateStart.clone().add(item, 'days'));
    });

    return dates;
  }

  /**
   *
   */
  timeout() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 8000);
    });
  }
}

module.exports = DriverExceptions;

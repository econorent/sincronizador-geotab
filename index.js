const API = require('mg-api-node');
const async = require('async');
const fs = require('fs');
const lodash = require('lodash');
const moment = require('moment-timezone');
const odbc = require('odbc');
const path = require('path');

moment.locale('es');

require('dotenv').config({
  path: path.join(__dirname, '.env'),
});

const DB = require('./db/index');
const ErrorEnhanced = require('./bin/error-enhanced');
const logger = require('./bin/log');

//
const GeoTab = require('./bin/geotab');
const db = new DB();

/**
 * Pants module.
 * @module sincronizador
 * @see module:sinscronizador
 */
(async () => {
  try {
    await db.init();

    await setComplements();

    let service = null;

    getAllControllers(path.join(__dirname, 'controller'))
      .map((file) => {
        return require(file);
      })
      .forEach((controller) => {
        service = new controller(GeoTab);

        service.init();
      });
  } catch (error) {
    logger.error(error);
  }
})();

/**
 * Asigna los complementos necesarios al proceso del sistema.
 */
async function setComplements() {
  process.path = path;
  process.fs = fs;
  process.async = async;
  process.lodash = lodash;
  process.moment = moment;
  process.odbc = odbc;
  process.API = API;
  process.DB = db.db;
  process.ErrorEnhanced = ErrorEnhanced;
  process.logger = logger;
}

/**
 * Obtiene todos los controladores del sistema.
 */
function getAllControllers (dirPath, arrayOfFiles = []) {
  fs.readdirSync(dirPath).forEach((file) => {
    if (fs.statSync(dirPath + '/' + file).isDirectory()) {
      getAllControllers(path.join(dirPath, '/', file), arrayOfFiles);
    } else {
      arrayOfFiles.push(path.join(dirPath, '/', file));
    }
  });

  return arrayOfFiles;
}

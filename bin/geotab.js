
const Main = require('./main');

class GeoTab extends Main {
  constructor() {
    super();
    this.api = null;
  }

  init() {
    return new Promise((resolve, reject) => {
      this.api = new this.API(
        process.env.GEOTAB_USER,
        process.env.GEOTAB_PASSWORD,
        process.env.GEOTAB_DATABASE,
        process.env.GEOTAB_SERVER
      );

      // eslint-disable-next-line no-unused-vars
      this.api.authenticate((error, result) => {
        if (error) {
          reject(error);
          return;
        }

        this.logger.info('Servicio geotab iniciado correctamente.');
        resolve();
      });
    });
  }

  GetGeoTabDevices(deviceName) {
    const search = deviceName ? {
      name: deviceName,
    } : null;
    const resultsLimit = deviceName ? 1 : null;

    return new Promise((resolve, reject) => {
      this.api.call('Get', {
        typeName: 'Device',
        resultsLimit,
        search,
      }, (error, devicesResult) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(devicesResult);
      });
    });
  }

  GetGeoTabRule(ruleName) {
    return new Promise((resolve, reject) => {
      this.api.call('Get', {
        typeName: 'Rule',
        search: {
          name: ruleName,
        },
        resultsLimit: 1,
      }, (error, ruleResult) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(ruleResult[0]);
      });
    });
  }

  GeoTabExceptionsEvent(rule, fromDate, toDate) {
    return new Promise((resolve, reject) => {
      this.api.call('Get', {
        typeName: 'ExceptionEvent',
        search: {
          ruleSearch: {
            id: rule.id,
          },
          fromDate,
          toDate,
        },
      }, (error, exceptionsResult) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(exceptionsResult);
      });
    });
  }

  GetRoadMaxSpeeds(device, fromDate, toDate) {
    return new Promise((resolve, reject) => {
      this.api.call('GetRoadMaxSpeeds', {
        deviceSearch: {
          id: device.id,
        },
        fromDate,
        toDate,
      }, (error, roadMaxSpeedsResult) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(roadMaxSpeedsResult);
      });
    });
  }

  GetLogRecord(fromDate, toDate, device) {
    return new Promise((resolve, reject) => {
      this.api.call('Get', {
        typeName: 'LogRecord',
        search: {
          deviceSearch: {
            id: device.id,
          },
          fromDate,
          toDate,
        },
      }, (error, logResult) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(logResult);
      });
    });
  }

  GetAddresses(latitude, longitude) {
    return new Promise((resolve, reject) => {
      this.api.call('GetAddresses', {
        coordinates: [
          {
            x: `${ longitude }`,
            y: `${ latitude }`,
          },
        ],
        movingAddresses: true,
      }, (error, addressResult) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(addressResult);
      });
    });
  }

  GetMultiCall(apiCalls) {
    return new Promise((resolve, reject) => {
      this.api.multicall(apiCalls, (error, result) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(this._.flattenDeep(result));
      });
    });
  }
}

module.exports = GeoTab;

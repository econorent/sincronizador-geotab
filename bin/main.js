
class Main {
  constructor() {
    this.path = process.path;
    this.fs = process.fs;
    this.async = process.async;
    this._ = process.lodash;
    this.moment = process.moment;
    this.odbc = process.odbc;
    this.API = process.API;
    this.db = process.DB;
    this.ErrorEnhanced = process.ErrorEnhanced;
    this.logger = process.logger;
  }
}

module.exports = Main;

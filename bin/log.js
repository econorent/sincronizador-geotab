const { createLogger, format, transports, } = require('winston');
const path = require('path');

require('winston-daily-rotate-file');

const combinedTransport = new (transports.DailyRotateFile)({
  filename: '%DATE%-all.log',
  dirname: path.join(__dirname, '..', 'log'),
  datePattern: process.env.DATE_FORMAT_SHORT,
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '30d',
  auditFile: path.join(__dirname, '..', 'log', 'audit.json'),
});
const errorTransport = new (transports.DailyRotateFile)({
  level: 'error',
  filename: '%DATE%-error.log',
  dirname: path.join(__dirname, '..', 'log'),
  datePattern: process.env.DATE_FORMAT_SHORT,
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '30d',
  auditFile: path.join(__dirname, '..', 'log', 'audit.json'),
});

const logger = createLogger({
  level: 'silly',
  format: format.combine(
    format.timestamp(),
    format.splat(),
    format.align(),
    format.printf((info) => {
      return `${info.timestamp} ${info.level}: ${info.message}`;
    })
  ),
  transports: [
    combinedTransport,
    errorTransport,
  ],
});

if (process.env.APP_ENV === 'DEVELOPMENT' || process.env.APP_ENV === 'TEST') {
  logger.add(new transports.Console({
    level: 'info',
    format: format.combine(
      format.colorize(),
      format.splat(),
      format.timestamp(),
      format.align(),
      format.printf((info) => {
        return `${info.timestamp} ${info.level}: ${info.message}`;
      })
    ),
  }));
}

module.exports = logger;
